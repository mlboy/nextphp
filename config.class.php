<?php
$config=array(
	//数据库设置
	'db'=>array(
		'dsn'=>'mysql:host=localhost;dbname=web;',	//PDO数据库连接字符串
		'user'=>'root',
		'pass'=>'dream',
		'prefix'=>'',			//表前缀
	),
	//路由设置
	'router'=>array(
		array('path'=>'get#show/:str','action'=>'index/show'),
		array('path'=>'get#user/:id','action'=>'user/show'),
	),
);
