<?php
define('DEBUG',false);							//是否开启debug
define('TPL_CACHE_SET',false);						//设置缓存开启
define('TPL_CACHE_TIME','60*60*12');						//设置缓存的时间
define('TPL_SUFFIX','html');						//模版后缀
define('APP_NAME','home');

define('IN_CORE',true);
require(dirname(__FILE__).'/core/core.php');	//框架核心文件
c::run();