<?php
if (!defined('IN_CORE')) die('禁止访问!');
	class model {
		protected $db=null;
		public static $query=null;
		public function __construct($name=null)
		{
			if(is_null($name)){
				$this->db=$this->getNotORM();
			}else{
				$db=$this->getNotORM();
				$this->db=$db->$name ();
			}
		}
		protected  function getNotORM(){
			//载入数据库
			if(isset(core::$config['db']['dsn'])){
				include CORE_PATH.DS.'NotORM.php';
				$pdo = new PDO(core::$config['db']['dsn'],core::$config['db']['user'],core::$config['db']['pass']);
				$pdo->query('set names utf8');
				$structure = new NotORM_Structure_Convention(
					$primary = 'id',
					$foreign = '%s_id',
					$table = '%s',
					$prefix = core::$config['db']['prefix']
				);
				if(defined('DEBUG') && DEBUG==true){
					$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$cache=null;
				}else{
					$cache=core::cache('table');
				}
				$cache=core::cache('table');
				$db = new NotORM($pdo,$structure,$cache);
				if(defined('DEBUG') && DEBUG==true){
					$db->debug = function ($query, $parameters){
						foreach($parameters as $v){
							$query=preg_replace('*\?*',$v,$query,1);
						}
						core::addmsg("{$query}",2);
						return true;
					};
					core::addmsg("<b>载入</b> [NotORM.php] 文件", 1);
				}
			}
			return $db;
		}
		public function __get($name){
			return $this->$name;
		}
		public function toArray($data){
			(!$data)&& $data=$this->db;
			return array_map('iterator_to_array', iterator_to_array($data));
		}
		// public function save($key,$value){
			// $cache=c::cache('data');
			// $cache->save($key,$value);
			// return true;
		// }
		// public function load($key){
			// $cache=c::cache('data');
			// $cache=$cache->load($key);
			// return $cache;
		// }
	}
