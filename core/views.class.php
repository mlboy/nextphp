<?php
if (!defined('IN_CORE')) die('禁止访问!');
	class views extends Smarty {
		/**
		 * 构造方法，用于初使化Smarty对象中的成员属性
		 *
		 */
		function __construct(){
			$this->template_dir	=APP_PATH.DS.'views'.DS;  //模板目录
			$this->compile_dir	=ROOT_PATH.DS.'runtime'.DS.APP_NAME.DS.'comps';    //里的文件是自动生成的，合成的文件
			$this->cache_dir	=ROOT_PATH.DS.'runtime'.DS.APP_NAME.DS.'cache'; 	 //设置缓存的目录
			$this->caching=TPL_CACHE_SET;     //设置缓存开启
			if (!defined('IN_CORE')) die('禁止访问!');
			$this->cache_lifetime=TPL_CACHE_TIME;  //设置缓存的时间 
			$this->left_delimiter="<{";   //模板文件中使用的“左”分隔符号
			$this->right_delimiter="}>";   //模板文件中使用的“右”分隔符号
			parent::__construct();        //调用父类被覆盖的构造方法
		}

		/*
		 * 重载父类Smarty类中的方法
		 * @param	string	$resource_name	模板的位置
		 * @param	mixed	$cache_id	缓存的ID
		 */
		function display($resource_name=null, $cache_id = null, $compile_id = null) {
			//将部分全局变量直接分配到模板中使用
			$this->assign('sroot',$GLOBALS['sroot']);	//Web服务器根到项目的根
			$this->assign('root',$GLOBALS['root']);	//Web服务器根到项目的根带http
			$this->assign('app',$GLOBALS['app']);	//app服务器根到项目的根
			$this->assign("public",$GLOBALS['public']);  //系统public公共目录
			$this->assign("res",$GLOBALS['res']);  //应用 资源目录
			$this->assign("url",$GLOBALS['url']);  //应用 脚本文件
			$this->assign("act",$GLOBALS['act']);  //应用 当前动作
			if(is_null($resource_name)){
				$resource_name=core::$action.'/'.core::$method.'.'.TPL_SUFFIX;
			}else if(strstr($resource_name,'/')){
				$resource_name=$resource_name.'.'.TPL_SUFFIX;
			}else{
				$resource_name=core::$action.'/'.$resource_name.'.'.TPL_SUFFIX;
			}
			Debug::addmsg("<b>载入</b> [{$resource_name}] 文件", 1);
			parent::display($resource_name, $cache_id, $compile_id);	
		}
		/* 
		 * 重载父类的Smarty类中的方法
		 * @param	string	$tpl_file	模板文件
		 * @param	mixed	$cache_id	缓存的ID
		 */
		function is_cached($tpl_file=null, $cache_id = null, $compile_id = null) {
			if(is_null($tpl_file)){
				$tpl_file=core::$action.'/'.core::$method.'.'.TPL_SUFFIX;
			}else if(strstr($tpl_file,'/')){
				$tpl_file=$resource_name.'.'.TPL_SUFFIX;
			}else{
				$tpl_file=core::$action.'/'.$resource_name.'.'.TPL_SUFFIX;
			}
			return parent::is_cached($tpl_file, $cache_id, $compile_id);
		}

		/* 
		 * 重载父类的Smarty类中的方法
		 *  @param	string	$tpl_file	模板文件
		 * @param	mixed	$cache_id	缓存的ID
		 */

		function clear_cache($tpl_file = null, $cache_id = null, $compile_id = null, $exp_time = null){
			if(is_null($tpl_file)){
				$tpl_file=core::$action.'/'.core::$method.'.'.TPL_SUFFIX;
			}else if(strstr($tpl_file,'/')){
				$tpl_file=$resource_name.'.'.TPL_SUFFIX;
			}else{
				$tpl_file=core::$action.'/'.$resource_name.'.'.TPL_SUFFIX;
			}
			return parent::clear_cache($tpl_file = null, $cache_id = null, $compile_id = null, $exp_time = null);
		}
	}
